(ns plf02.core)

;;-----------------Funcion associative?---------------
(defn associativo?-1 [x]
  (associative? x))

(associativo?-1 {1 2 3 4})
(associativo?-1 #{10 29 38})
(associativo?-1 [1 10 2 20])

(defn es-asociativo-2? [x]
  (if (associative? x) "Si es asociativo" "No es asociativo"))
(es-asociativo-2? {1 2 3 4})
(es-asociativo-2? [10 20 30 40])
(es-asociativo-2? #{100 200 300})

(defn son-asosiativos-3? [x y]
  (if (and (associative? x) (associative? y)) "Si" "No"))

(son-asosiativos-3? {1 2} [10 20])
(son-asosiativos-3? #{100 200 300} [10 20 30 40])
(son-asosiativos-3? {1 2 3 4} #{100 200 300})

;;-----------------Funcion boolean?---------------
(defn es-boolean?-1 [x]
  (if (boolean? x) "si" "no"))
(es-boolean?-1 true)
(es-boolean?-1 (new Boolean "verdad"))
(es-boolean?-1 "nil")

(defn son-booleanos?-2 [x y]
  (if (and (boolean? x) (boolean? y)) "Ambos son booleanos" "Algun(os) argumento(os) no son booleanos"))
(son-booleanos?-2 true false)
(son-booleanos?-2 true 4)
(son-booleanos?-2 false (new Boolean "Si"))

(defn cuantos-booleanos?-3 [x]
  (count (filter boolean? x)))
(cuantos-booleanos?-3 [true 1 false 3])
(cuantos-booleanos?-3 #{true false 1 2 3 4 5})
(cuantos-booleanos?-3 {true true false false})

;;-----------------Funcion char?---------------
(defn caracter?-1 [x]
  (if (char? x) "Si" "No"))
(caracter?-1 \a)
(caracter?-1 2)
(caracter?-1 (char 97))

(defn son-caracter?-2 [x]
  (map char? x))
(son-caracter?-2 [\a \b \c])
(son-caracter?-2 ["hola" "adios"])
(son-caracter?-2 [(char 97) (char 98)])

(defn cuantos-caracteres?-3 [x]
  (count (filter char? x)))
(cuantos-caracteres?-3 "hola")
(cuantos-caracteres?-3 [\a \b \c \d])
(cuantos-caracteres?-3 [\a \b \c \d true false])


;;-----------------Funcion coll?---------------
(defn es-coleccion?-1 [x]
  (if (coll? x) "Si" "No"))
(es-coleccion?-1 [])
(es-coleccion?-1 {})
(es-coleccion?-1 #{})

(defn dos-colecciones?-2 [x y]
  (if (and (coll? x) (coll? y)) "Si" "No"))
(dos-colecciones?-2 [] {})
(dos-colecciones?-2 {} [])
(dos-colecciones?-2 #{} {})

(defn cuantas-colecciones?-3 [x]
  (count (filter coll? x)))
(cuantas-colecciones?-3 [[] [] []])
(cuantas-colecciones?-3 [[] {} {}])
(cuantas-colecciones?-3 [[] #{} {} \a])

;;-----------------Funcion decimal?---------------
(defn es-decimal?-1 [x]
  (if (decimal? x) "Si" "No"))
(es-decimal?-1 10)
(es-decimal?-1 10.1)
(es-decimal?-1 1.9M)

(defn son-decimales?-2 [x y]
  (if (and (decimal? x) (decimal? y)) "Si" "No"))
(son-decimales?-2 2.2 1.999)
(son-decimales?-2 2.1M 2.2M)
(son-decimales?-2 2.1M 2.999999)

(defn contar-decimales-3 [x]
  (count (filter decimal? x)))
(contar-decimales-3 [2.1M 2.2M 1.9M])
(contar-decimales-3 [2.2M 2.2 1.9999 9.1M])
(contar-decimales-3 [3.9 0.11111 0.0000001 1.1 0.1M 999.999])

;;-----------------Funcion double?---------------
(defn es-double?-1 [x]
  (if (double? x) "Si" "No"))
(es-double?-1 10)
(es-double?-1 10.1)
(es-double?-1 1.9)

(defn dos-double?-2 [x y]
  (if (and (double? x) (double? y)) "Si" "No"))
(dos-double?-2 2.2 1.999)
(dos-double?-2 2.1M 2.2M)
(dos-double?-2 2.1 2.999999)

(defn contar-double-3 [x]
  (count (filter double? x)))
(contar-double-3 [2.1M 2.2M 1.9M])
(contar-double-3 [2.2M 2.2 1.9999 9.1M])
(contar-double-3 [3.9 0.11111 0.0000001 1.1 0.1M 999.999])

;;-----------------Funcion float?---------------
(defn es-float?-1 [x]
  (if (float? x) "Si" "No"))
(es-float?-1 100)
(es-float?-1 10.12)
(es-float?-1 1.77M)

(defn dos-flotantes?-2 [x y]
  (if (and (float? x) (float? y)) "Si" "No"))
(dos-flotantes?-2 2.2 1.999)
(dos-flotantes?-2 2.1M 2.2M)
(dos-flotantes?-2 2.1M 2.999999)

(defn contar-flotantes-3 [x]
  (count (filter float? x)))
(contar-flotantes-3 [2.1M 2.2M 1.9M])
(contar-flotantes-3 [2.2M 2.2 1.9999 9.1M])
(contar-flotantes-3 [3.9 0.11111 0.0000001 1.1 0.1M 999.999])

;;-----------------Funcion indent?---------------
(defn es-ident?-1 [x]
  (if (ident? x) "Si" "No"))
(es-ident?-1 "hola")
(es-ident?-1 :key)
(es-ident?-1 'hola)

(defn dos-ident?-2 [x y]
  (if (and (ident? x) (ident? y)) "Si" "No"))
(dos-ident?-2 "ident" "hola")
(dos-ident?-2 :key1 :key2)
(dos-ident?-2 :key 'abc)

(defn contar-ident-3 [x]
  (count (filter ident? x)))
(contar-ident-3 [2.1M :key 1.9M 'abc])
(contar-ident-3 [2.2M 2.2 1.9999 9.1M])
(contar-ident-3 [3.9 "0.11111" :hola 1.1 0.1M '123])

;;---------------------indexed?---------------------------
(defn es-indexed?-1 [x]
  (if (indexed? x) "Si" "No"))
(es-indexed?-1 "hola")
(es-indexed?-1 [1 2 3 4])
(es-indexed?-1 {1 20 2 56 3 22})

(defn dos-indexed?-2 [x y]
  (if (and (indexed? x) (indexed? y)) "Si" "No"))
(dos-indexed?-2 "ident" "hola")
(dos-indexed?-2 [1 2 3 3] [2 4 6 8])
(dos-indexed?-2 :key [1 2 3 4])

(defn contar-indexed-3 [x]
  (count (filter indexed? x)))
(contar-indexed-3 [[1 2 3 4] [1 2 3 4] "hola"])
(contar-indexed-3 ["hola" "adios" {1 2 3 4}])
(contar-indexed-3 [{1 2 3 4} ["hola" "adios"] 1 2])

;;---------------------int?---------------------------
(defn es-int?-1 [x]
  (if (int? x) "Si" "No"))
(es-int?-1 "12")
(es-int?-1 20)
(es-int?-1 22.1)

(defn dos-int?-2 [x y]
  (if (and (int? x) (int? y)) "Si" "No"))
(dos-int?-2 "5" "10")
(dos-int?-2 20 7)
(dos-int?-2 21 20.0)

(defn contar-int-3 [x]
  (count (filter int? x)))
(contar-int-3 [1 2 3 4 5])
(contar-int-3 [2.2M 2.2 1.9999 9.1M])
(contar-int-3 [3.9 "0.11111" :hola 1.1 0.1M 123])

;;---------------------integer?---------------------------
(defn es-integer?-1 [x]
  (if (integer? x) "Si" "No"))
(es-integer?-1 "12")
(es-integer?-1 20)
(es-integer?-1 22.1)

(defn dos-integer?-2 [x y]
  (if (and (integer? x) (integer? y)) "Si" "No"))
(dos-integer?-2 "5" "10")
(dos-integer?-2 20 7)
(dos-integer?-2 21 20.0)

(defn contar-integer-3 [x]
  (count (filter integer? x)))
(contar-integer-3 [1 2 3 4 5])
(contar-integer-3 [2.2M 2.2 1.9999 9.1M])
(contar-integer-3 [3.9 "0.11111" :hola 1.1 0.1M 123])

;;---------------------keyword?---------------------------
(defn es-keyword?-1 [x]
  (if (keyword? x) "Si" "No"))
(es-keyword?-1 ":12")
(es-keyword?-1 :hola)
(es-keyword?-1 true)

(defn dos-keyword?-2 [x y]
  (if (and (keyword? x) (keyword? y)) "Si" "No"))
(dos-keyword?-2 "keyword" :key)
(dos-keyword?-2 :20 :7)
(dos-keyword?-2 :value :key)

(defn contar-keyword-3 [x]
  (count (filter keyword? x)))
(contar-keyword-3 [:1 :2 :3 :4 :5])
(contar-keyword-3 [2.2M 2.2 1.9999 9.1M])
(contar-keyword-3 [3.9 "0.11111" :hola 1.1 0.1M 123])

;;---------------------list?---------------------------
(defn es-list?-1 [x]
  (if (list? x) "Si" "No"))
(es-list?-1 '(1 2 3))
(es-list?-1 #(1 2 3))
(es-list?-1 {:hola "adios" :no "false"})

(defn dos-list?-2 [x y]
  (if (and (list? x) (list? y)) "Si" "No"))
(dos-list?-2 '(1 2 3 4) {:1 "hola"})
(dos-list?-2 '(1 2 3) '(4 5 6))
(dos-list?-2 21 20.0)

(defn contar-list-3 [x]
  (count (filter list? x)))
(contar-list-3 ['(1 2 3 4) {:1 "hola"}])
(contar-list-3 ['(1 2 3) '(4 5 6)])
(contar-list-3 [3.9 "0.11111" :hola 1.1 0.1M 123])

;;---------------------map-entry?---------------------------
(defn es-map-entry?-1 [x]
  (if (map-entry? x) "Si" "No"))
(es-map-entry?-1 (first {:a 1 :b 2}))
(es-map-entry?-1 {:a 1})
(es-map-entry?-1 {:hola "adios" :no "false"})

(defn dos-map-entry?-2 [x y]
  (if (and (map-entry? x) (map-entry? y)) "Si" "No"))
(dos-map-entry?-2 (first {:a 1 :b 2}) (last {:a 1 :b 2}))
(dos-map-entry?-2 (first [1 2 3 4]) (last {:a 1 :b 2}))
(dos-map-entry?-2 :21 20.0)

(defn contar-map-entry-3 [x]
  (count (filter map-entry? x)))
(contar-map-entry-3 ['(1 2 3 4) {:1 "hola"}])
(contar-map-entry-3 [(first {:a 1 :b 2}) (last {:a 1 :b 2})])
(contar-map-entry-3 [3.9 "0.11111" :hola 1.1 0.1M 123])

;;---------------------map?---------------------------
(defn es-map?-1 [x]
  (if (map? x) "Si" "No"))
(es-map?-1 (first {:a 1 :b 2}))
(es-map?-1 {:a 1})
(es-map?-1 {:hola "adios" :no "false"})

(defn dos-map?-2 [x y]
  (if (and (map? x) (map? y)) "Si" "No"))
(dos-map?-2 (first {:a 1 :b 2}) (last {:a 1 :b 2}))
(dos-map?-2 {:1 "hola"} {:a 1 :b 2})
(dos-map?-2 :21 20.0)

(defn contar-map-3 [x]
  (count (filter map? x)))
(contar-map-3 ['(1 2 3 4) {:1 "hola"}])
(contar-map-3 [(first {:a 1 :b 2}) (last {:a 1 :b 2})])
(contar-map-3 [{:2 "hola"} {:2 "adios"} [1 2 3]])

;;---------------------nat-int?---------------------------
(defn es-entero-natural?-1 [x]
  (if (nat-int? x) "Si" "No"))
(es-entero-natural?-1 "12")
(es-entero-natural?-1 20)
(es-entero-natural?-1 -22.1)
-1
(defn dos-nat-int?-2 [x y]
  (if (and (nat-int? x) (nat-int? y)) "Si" "No"))
(dos-nat-int?-2 "5" "10")
(dos-nat-int?-2 20 7)
(dos-nat-int?-2 21 -200)

(defn contar-nat-int-3 [x]
  (count (filter nat-int? x)))
(contar-nat-int-3 [1 2 3 4 5])
(contar-nat-int-3 [2.2M 2.2 1.9999 9.1M])
(contar-nat-int-3 [3.9 "0.11111" :hola 1.1 0.1M 123])

;;---------------------number?---------------------------
(defn es-numero?-1 [x]
  (if (number? x) "Si" "No"))
(es-numero?-1 "12")
(es-numero?-1 20)
(es-numero?-1 22.1)

(defn dos-numeros?-2 [x y]
  (if (and (number? x) (number? y)) "Si" "No"))
(dos-numeros?-2 "5" "10")
(dos-numeros?-2 20 7)
(dos-numeros?-2 21 20.0)

(defn contar-numeros-3 [x]
  (count (filter number? x)))
(contar-numeros-3 [1 2 3 4 5])
(contar-numeros-3 [2.2M 2.2 1.9999 9.1M])
(contar-numeros-3 [3.9 "0.11111" :hola 1.1 0.1M 123])

;;---------------------pos-int?---------------------------
(defn es-entero-positivo?-1 [x]
  (if (pos-int? x) "Si" "No"))
(es-entero-positivo?-1 "12")
(es-entero-positivo?-1 20)
(es-entero-positivo?-1 -22)

(defn dos-enteros-positivos?-2 [x y]
  (if (and (pos-int? x) (pos-int? y)) "Si" "No"))
(dos-enteros-positivos?-2 "5" "10")
(dos-enteros-positivos?-2 20 7)
(dos-enteros-positivos?-2 21.0 -20)

(defn contar-enteros-positivos-3 [x]
  (count (filter pos-int? x)))
(contar-enteros-positivos-3 [1 2 3 4 5])
(contar-enteros-positivos-3 [2.2M 2.2 1.9999 9.1M])
(contar-enteros-positivos-3 [3.9 "0.11111" :hola 1.1 0.1M 123])

;;---------------------ratio?---------------------------
(defn es-ratio?-1 [x]
  (if (ratio? x) "Si" "No"))
(es-ratio?-1 "12/4")
(es-ratio?-1 22/7)
(es-ratio?-1 0.5)

(defn dos-ratios?-2 [x y]
  (if (and (ratio? x) (ratio? y)) "Si" "No"))
(dos-ratios?-2 "5" "10")
(dos-ratios?-2 20 7)
(dos-ratios?-2 22/7 1/3)

(defn contar-ratios-3 [x]
  (count (filter ratio? x)))
(contar-ratios-3 [1 22/7 1/3 4/5])
(contar-ratios-3 [2.2M 2.2 1.9999 9.1M])
(contar-ratios-3 [3.9 22/7 1/3 4/5 1.1 0.1M 123])

;;---------------------rational?---------------------------
(defn es-racional?-1 [x]
  (if (rational? x) "Si" "No"))
(es-racional?-1 "12/4")
(es-racional?-1 22/7)
(es-racional?-1 0.5)

(defn dos-racionales?-2 [x y]
  (if (and (rational? x) (rational? y)) "Si" "No"))
(dos-racionales?-2 "5" "10")
(dos-racionales?-2 20 7)
(dos-racionales?-2 22/7 1/3)

(defn contar-racionales-3 [x]
  (count (filter rational? x)))
(contar-racionales-3 [1 22/7 1/3 4/5])
(contar-racionales-3 [2.2M 2.2 1.9999 9.1M])
(contar-racionales-3 [3.9 22/7 1/3 4/5 1.1 0.1M 123])

;;---------------------seq?---------------------------
(defn es-seq?-1 [x]
  (if (seq? x) "Si" "No"))
(es-seq?-1 (range 1 5))
(es-seq?-1 '(1 2 3 4))
(es-seq?-1 [1 3 2 5])

(defn dos-rseq?-2 [x y]
  (if (and (seq? x) (seq? y)) "Si" "No"))
(dos-rseq?-2 (range 1 5) '(5 6 7 8))
(dos-rseq?-2 '(1 3 4 6 1) '(10 20 30))
(dos-rseq?-2 '(1 "hola" 2 "adios") '(7 9 4))

(defn contar-rseq-3 [x]
  (count (filter seq? x)))
(contar-rseq-3 [1 22/7 1/3 4/5])
(contar-rseq-3 ['(1 2) '(3 4)])
(contar-rseq-3 ['(1 2 5 6) 1.1 0.1M 123])

;;---------------------seqable?---------------------------
(defn es-seqable?-1 [x]
  (if (seqable? x) "Si" "No"))
(es-seqable?-1 \a)
(es-seqable?-1 nil)
(es-seqable?-1 "hola")

(defn dos-seqable?-2 [x y]
  (if (and (seqable? x) (seqable? y)) "Si" "No"))
(dos-seqable?-2 (range 1 5) '(5 6 7 8))
(dos-seqable?-2 '(1 3 4 6 1) '(10 20 30))
(dos-seqable?-2 '(1 "hola" 2 "adios") \a)

(defn contar-seqable-3 [x]
  (count (filter seqable? x)))
(contar-seqable-3 [1 22/7 1/3 4/5])
(contar-seqable-3 ['(1 2) '(3 4)])
(contar-seqable-3 ['(1 2 5 6) 1.1 0.1M 123])

;;---------------------sequential?---------------------------
(defn es-sequential?-1 [x]
  (if (sequential? x) "Si" "No"))
(es-sequential?-1 '(\a \b c))
(es-sequential?-1 [1 2 3 4])
(es-sequential?-1 {4 5 6 7})

(defn dos-secuenciales?-2 [x y]
  (if (and (sequential? x) (sequential? y)) "Si" "No"))
(dos-secuenciales?-2 (range 1 5) '(5 6 7 8))
(dos-secuenciales?-2 '(1 3 4 6 1) '(10 20 30))
(dos-secuenciales?-2 '(1 "hola" 2 "adios") \a)

(defn contar-secuenciales-3 [x]
  (count (filter sequential? x)))
(contar-secuenciales-3 [1 22/7 1/3 4/5])
(contar-secuenciales-3 ['(1 2) '(3 4)])
(contar-secuenciales-3 ['(1 2 5 6) 1.1 0.1M 123])


;;---------------------set?---------------------------
(defn es-set?-1 [x]
  (if (set? x) "Si" "No"))
(es-set?-1 '(\a \b c))
(es-set?-1 [1 2 3 4])
(es-set?-1 #{4 5 6 7})

(defn dos-set?-2 [x y]
  (if (and (set? x) (set? y)) "Si" "No"))
(dos-set?-2 #{} (sorted-set 1 2 3))
(dos-set?-2 '(1 3 4 6 1) '(10 20 30))
(dos-set?-2 '(1 "hola" 2 "adios") #{})

(defn contar-set-3 [x]
  (count (filter set? x)))
(contar-set-3 [#{} (sorted-set 1 2) [1 2 3]])
(contar-set-3 ['(1 2) '(3 4)])
(contar-set-3 ['(1 2 5 6) 1.1 0.1M 123])

;;---------------------some?---------------------------
(defn hay-algo?-1 [x]
  (if (some? x) "Si" "No"))
(hay-algo?-1 '(\a \b c))
(hay-algo?-1 nil)
(hay-algo?-1 "no")

(defn dos-some-2 [x y]
  (if (and (some? x) (some? y)) "Si" "No"))
(dos-some-2 (range 1 5) '(5 6 7 8))
(dos-some-2 '(1 3 4 6 1) '(10 20 30))
(dos-some-2 '(1 "hola" 2 "adios") nil)

(defn contar-some-3 [x]
  (count (filter some? x)))
(contar-some-3 [1 22/7 1/3 4/5])
(contar-some-3 ['(1 2) '(3 4)])
(contar-some-3 [nil nil 1 nil 2 nil 4 nil 3])

;;---------------------string?---------------------------
(defn es-string?-1 [x]
  (if (string? x) "Si" "No"))
(es-string?-1 "12")
(es-string?-1 20)
(es-string?-1 "hola amigos")

(defn dos-string?-2 [x y]
  (if (and (string? x) (string? y)) "Si" "No"))
(dos-string?-2 "5" "10")
(dos-string?-2 20 7)
(dos-string?-2 21 "20.0")

(defn contar-string-3 [x]
  (count (filter string? x)))
(contar-string-3 ["1 2 3 4 5" "Hola" \a 12])
(contar-string-3 [2.2M 2.2 1.9999 9.1M])
(contar-string-3 [3.9 "0.11111" :hola 1.1 0.1M 123])

;;---------------------symbol?---------------------------
(defn es-symbol?-1 [x]
  (if (symbol? x) "Si" "No"))
(es-symbol?-1 \a)
(es-symbol?-1 2)
(es-symbol?-1 'a)

(defn dos-symbol?-2 [x y]
  (if (and (symbol? x) (symbol? y)) "Si" "No"))
(dos-symbol?-2 'a 'b)
(dos-symbol?-2 '7 'b)
(dos-symbol?-2 'p \p)

(defn contar-symbol-3 [x]
  (count (filter symbol? x)))
(contar-symbol-3 [1 2 3 4 5])
(contar-symbol-3 ['a 'b 'b 'c 'y])
(contar-symbol-3 ['a \b :c])

;;---------------------vector---------------------------
(defn es-vector?-1 [x]
  (if (vector? x) "Si" "No"))
(es-vector?-1 '(\a \b c))
(es-vector?-1 [1 2 3 4])
(es-vector?-1 #{4 5 6 7})

(defn dos-vectores?-2 [x y]
  (if (and (vector? x) (vector? y)) "Si" "No"))
(dos-vectores?-2 #{} (sorted-set 1 2 3))
(dos-vectores?-2 [] [])
(dos-vectores?-2 '(1 "hola" 2 "adios") #{})

(defn contar-vectores-3 [x]
  (count (filter vector? x)))
(contar-vectores-3 [#{} (sorted-set 1 2) [1 2 3]])
(contar-vectores-3 [[] [] [] {}])
(contar-vectores-3 ['(1 2 5 6) 1.1 0.1M 123])

;;=====================FUNCIONES DE ORDEN SUPERIOR=======================
;;--------------------------drop..................................
(defn dropn-1 [n] 
  (drop n))
(dropn-1 5)
(dropn-1 10)
(dropn-1 30)

(defn drop-n-coll-2 [n coll]
  (drop n coll))
(drop-n-coll-2 1 [1 2 3 4])
(drop-n-coll-2 2 [1 2 3])
(drop-n-coll-2 1 ["hola" "adios" "buenas" "tardes"])

(defn quitar-primer-3 [coll]
  (drop 1 coll))
(quitar-primer-3 [1 2 3 4])
(quitar-primer-3 [1 2 3])
(quitar-primer-3 ["hola" "adios" "buenas" "tardes"])
;;--------------------------drop-last..................................
(defn quitar-ultimo-1 [n]
  (drop-last 1 n))
(quitar-ultimo-1 [1 2 3 4])
(quitar-ultimo-1 [1 2 3])
(quitar-ultimo-1 ["hola" "adios" "buenas" "tardes"])

(defn quitar-n-ultimos-2 [n coll]
  (drop-last n coll))
(quitar-n-ultimos-2 1 [1 2 3 4])
(quitar-n-ultimos-2 2 [1 2 3])
(quitar-n-ultimos-2 3 ["hola" "adios" "buenas" "tardes"])

(defn quitar-dos-ultimos-3 [coll]
  (drop-last 2 coll))
(quitar-dos-ultimos-3 [1 2 3 4])
(quitar-dos-ultimos-3 [1 2 3])
(quitar-dos-ultimos-3 ["hola" "adios" "buenas" "tardes"])

;;--------------------------drop-while..................................
(defn quitar-negativos-1 [coll]
  (drop-while neg? coll))
(quitar-negativos-1 [-1 -2 3 4])
(quitar-negativos-1 [1 2 3 -4 -5 -6])
(quitar-negativos-1 [-1 -2 -4 -6 -1 3 7 3])

(defn quitar-n-ceros-2 [coll]
  (drop-while zero? coll))
(quitar-n-ceros-2 [0 0 0 1 2 3 4])
(quitar-n-ceros-2 [0 0 10 1 2 3])
(quitar-n-ceros-2 [01 02 03])

(defn quitar-nil-3 [coll]
  (drop-while nil? coll))
(quitar-nil-3 [nil nil 1 2 3 4])
(quitar-nil-3 [1 2 nil 3])
(quitar-nil-3 [0 0.0 nil])

;;--------------------------every?..................................
(defn todos-positivos?-1 [n]
  (every? pos? n))
(todos-positivos?-1 [1 2 3 4])
(todos-positivos?-1 [1 2 3 -1])
(todos-positivos?-1 [0.0 0 1/2])

(defn todos-numeros?-2 [coll]
  (every? number? coll))
(todos-numeros?-2 [1 2 3 4])
(todos-numeros?-2 [1 2 3 12/4])
(todos-numeros?-2 ["hola" "adios" "buenas" "tardes" 1 2 3])

(defn todos-char-3 [coll]
  (every? char? coll))
(todos-char-3 [1 2 3 4])
(todos-char-3 [\a \b \c])
(todos-char-3 [\a \b \c (char 99)])

;;--------------------------filterv..................................
(defn filtrar-positivos?-1 [n]
  (filterv pos? n))
(filtrar-positivos?-1 [-1 2 3 4 0 -1 -20])
(filtrar-positivos?-1 [1 2 3 -1])
(filtrar-positivos?-1 [0.0 0 1/2])

(defn filtrar-numeros?-2 [coll]
  (filterv number? coll))
(filtrar-numeros?-2 [\a :1 2 3 4])
(filtrar-numeros?-2 [1 2 3 12/4])
(filtrar-numeros?-2 ["hola" "adios" "buenas" "tardes" 1 2 3])

(defn filtrar-char-3 [coll]
  (filterv char? coll))
(filtrar-char-3 [1 2 3 4 \a])
(filtrar-char-3 [\a \b \c])
(filtrar-char-3 [\a \b \c (char 99)])

;;--------------------------group-by..................................
(defn agrupar-longitud-1 [x]
  (group-by count x))
(agrupar-longitud-1 ["hi" "bye" "hey" "no"])
(agrupar-longitud-1 ["no" "si" "hola" "adios" "buenas" "noches"])
(agrupar-longitud-1 [" " " " "22" "33"])

(defn agrupar-tipo-2 [x]
  (group-by type x))
(agrupar-tipo-2 ["hola" 2 \a \b \v "adios" :1 2 :2 3 :3])
(agrupar-tipo-2 ["jugo" 2 \a \b \v "str" :1 2 4 6 8])
(agrupar-tipo-2 ["diente" 2 \a \b \v "muela" :2])

(defn agrupar-key-3 [key coll]
  (group-by key coll))
(agrupar-key-3 :user-id [{:user-id 1 :uri "/"}
 {:user-id 2 :uri "/foo"}
 {:user-id 1 :uri "/account"}])
;;--------------------------iterate..................................
(defn n-iteraciones-pares-1 [n]
  (take n (iterate inc 2)))
(n-iteraciones-pares-1 2)
(n-iteraciones-pares-1 10)
(n-iteraciones-pares-1 50)

(defn cinco-iteraciones-n-incremento-2 [n]
  (take 5 (iterate inc n)))
(cinco-iteraciones-n-incremento-2 2)
(cinco-iteraciones-n-incremento-2 5)
(cinco-iteraciones-n-incremento-2 10)

(defn cinco-iteraciones-n-decremento-3 [n]
  (take 5 (iterate dec n)))
(cinco-iteraciones-n-decremento-3 2)
(cinco-iteraciones-n-decremento-3 5)
(cinco-iteraciones-n-decremento-3 10)

;;--------------------------keep..................................
(defn keep-n-coll-1 [n coll]
  keep n coll)
(keep-n-coll-1 1 [2 3 4])
(keep-n-coll-1 2 [2 3 4 5 6 7])
(keep-n-coll-1 3 [nil 3 4 5])

;;--------------------------keep-indexed..................................
(defn keepindexed-1 [coll]
  (keep-indexed coll))
(keepindexed-1 [-1 0 1 2 3])
(keepindexed-1 [1 2 3])
(keepindexed-1 [1 2 3])

;;--------------------------map-indexed..................................
(defn vector-indexado-1 [mapa]
  (map-indexed vector mapa))
(vector-indexado-1 "1 2 3 4 5 6 11 22 33 44 55")
(vector-indexado-1 "hola")
(vector-indexado-1 [1 2 3])

(defn lista-indexada-2 [lista]
  (map-indexed list lista))
(lista-indexada-2 '(1 2 3 4))
(lista-indexada-2 '("hola" "adios"))
(lista-indexada-2 '(true false))

(defn set-indexado-3 [x]
  (map-indexed set x))
set-indexado-3 (set [1 1 4 6 23 1 8])
set-indexado-3 (set "hola")
set-indexado-3 (set '(\a \b \b \c \d))

;;--------------------------mapcat..................................
(defn mapcat-1 [coll]
  (mapcat list coll))
(mapcat-1 (1 [10 20 30]))

;;--------------------------mapv..................................
(defn inc-mapv-1 [col]
  (mapv inc col))
(inc-mapv-1 [1 2 3])
(inc-mapv-1 [10 20 30])
(inc-mapv-1 [-1 0 1])

(defn sum-mapv-2 [col1 col2]
  (mapv + col1 col2))
(sum-mapv-2 [1 2 3] [3 2 1])
(sum-mapv-2 [20.0 1 5] [20.1 3 6])
(sum-mapv-2 [2 6 8] [3 7 9])

(defn mul-mapv-3 [col1 col2]
  (mapv * col1 col2))
(mul-mapv-3 [1 2 3] [3 2 1])
(mul-mapv-3 [20.0 1 5] [20.1 3 6])
(mul-mapv-3 [2 6 8] [3 7 9])
;;--------------------------merge-with..................................
(defn mergewith-1 [m1 m2]
  (merge-with + m1 m2))
(mergewith-1 {:a 1  :b 2} {:a 9  :b 98 :c 0})
(mergewith-1 {:a 20 :b 30 :v 5} {:a -10 :b 3 :c 25})
(mergewith-1 {:a 12 :c 2} {:b 1 :a -2})

(defn mergewith-2 [m1 m2]
  (merge-with * m2 m1))
(mergewith-2 {:a 2 :b 1} {:a 82 :b 9})
(mergewith-2 {:a 4 :b 5} {:a 7 :b 7})
(mergewith-2 {:a 6 :b 0} {:a 3 :b 34 :c 2})

(defn mergewith-3 [m1 m2]
  (merge-with str m1 m2))
(mergewith-3 {:a "hola " :b "Humano"} {:a "mario" :b \s})
(mergewith-3 {:a "buenas " :b 3} {:a "tardes" :b " pesos"})
(mergewith-3 {:a "hasta " :b [1 2 3]} {:a "luego" :b [10 20 30]})
;;--------------------------not-any?..................................
(defn notany-nil-1 [x]
  (not-any? nil? x))
(notany-nil-1 [true false nil])
(notany-nil-1 [1 2 3 4 5 6])
(notany-nil-1 [nil 3 nil 6])

(defn notany-neg-2 [x]
  (not-any? neg? x))
(notany-neg-2 [2 3 5 7 40.1 -1])
(notany-neg-2 [0 2 4 5 6])
(notany-neg-2 [-1 -2 -3 -4])

(defn notany-cero-3 [x]
  (not-any? zero? x))
(notany-cero-3 [3 2 1 0 -1 -2 -3])
(notany-cero-3 [1 9 5 1 0\1 1])
(notany-cero-3 [0.1 0.2])
;;--------------------------not-every?..................................
(defn notevery-nil-1 [x]
  (not-every? nil? x))
(notevery-nil-1 [true false nil])
(notevery-nil-1 [1 2 3 4 5 6])
(notevery-nil-1 [nil nil])

(defn notevery-neg-2 [x]
  (not-every? neg? x))
(notevery-neg-2 [2 3 5 7 40.1 -1])
(notevery-neg-2 [0 2 4 5 6])
(notevery-neg-2 [-1 -2 -3 -4])

(defn notaeverycero-3 [x]
  (not-every? zero? x))
(notaeverycero-3 [0/1 0/2 0/3])
(notaeverycero-3 [1 9 5 1 0 \1 1])
(notaeverycero-3 [0.1 0.2])
;;--------------------------partition-by..................................
(defn partition-by-1 [x]
  (partition-by identity x))
(partition-by-1 "buenas")
(partition-by-1 [1 2 3 5])
(partition-by-1 '(\a \b \c))

(defn partition-by-2 [x] 
  (partition-by count x))
(partition-by-2 ["hola" "adios" "bye" "no" "si"])
(partition-by-2 [[12 13 14] [1 2 3] [10 20]])
(partition-by-2 ["hola" "hoja" "adios"])

(defn partition-by-3 [z]
  (partition-by even? z))
(partition-by-3 [1 1 2 1 3 4 7 2 4 1])
(partition-by-3 [0 1 3 5 0 2 4 1 5])
(partition-by-3 [0 1 2 5 1 3 5 6])
;;--------------------------reduce-kv..................................
(defn update-map-1 [m f]
  (reduce-kv (fn [m k v]
               (assoc m k (f v))) {} m))

(map #(update-map-1 % inc) [{:a 1 :b 2} {:a 3 :b 4}])
(map #(update-map-1 % inc) [{:a 4 :b 2} {:a 6 :b 8}])
(map #(update-map-1 % inc) [{:a 10 :b 20} {:a 30 :b 40}])
;;--------------------------remove..................................
(defn remove-pos-1 [x]
  (remove pos? x))
(remove-pos-1 [12 4 -6 7 3 0 -1 -2])
(remove-pos-1 [-2 -5 -6 -8 32 -1 3 4 6 2 5 6 23])
(remove-pos-1 [-8 32 -1 3 4 1])

(defn remove-neg-2 [x]
  (remove neg? x))
(remove-neg-2 [12 4 -6 7 3 0 -1 -2])
(remove-neg-2 [-2 -5 -6 -8 0 32 -1 3 4 6 0 2 5 6 23])
(remove-neg-2 [-8 0 32 -1 3 4 1])

(defn remove-zero-3 [x]
  (remove zero? x))
(remove-zero-3 [12 4 -6 7 3 0 -1 -2])
(remove-zero-3 [-2 -5 -6 -8 0 32 -1 3 4 6 0 2 5 6 23])
(remove-zero-3 [-8 0 32 -1 3 4 1])
;;--------------------------reverse..................................
(defn reverse-1 [x]
  (reverse x))
(reverse-1 "hola")
(reverse-1 '(1 2 3 4))
(reverse-1 [-8 0 32 -1 3 4 1])

(defn reverse-str-2 [string]
  (apply str (reverse string)))
(reverse-str-2 "hola")
(reverse-str-2 "adios")
(reverse-str-2 "buenas tardes")

(defn reverse-3 [x]
  (apply (reverse x)))
(reverse-str-2 "hola")
(reverse-str-2 '(1 2 3 4 5))
(reverse-str-2 [1 2 3 4 5])
;;--------------------------some..................................
(defn some-1 [x]
  (some zero? x))
(some-1 [12 4 -6 7 3 0 -1 -2])
(some-1 [-2 -5 -6 -8 0 32 -1 3 4 6 0 2 5 6 23])
(some-1 [-8 32 -1 3 4 1])

(defn some-2 [x]
  (some neg? x))
(some-2 [12 4 -6 7 3 0 -1 -2])
(some-2 [-2 -5 -6 -8 0 32 -1 3 4 6 0 2 5 6 23])
(some-2 [8 32 1 3 4 1])

(defn some-3 [x]
  (some pos? x))
(some-3 [12 4 -6 7 3 0 -1 -2])
(some-3 [-2 -5 -6 -8 0 32 -1 3 4 6 0 2 5 6 23])
(some-3 [-8 -1])

;;--------------------------sort-by..................................
(defn sort-by-1 [c]
  (sort-by count c))
(sort-by-1 ["dddd" "bb" "ccc" "a"])
(sort-by-1 ['(1 2) '(1) '(1 2 3)])
(sort-by-1 ["hola" "adios" "hi" "hoja"])

(defn sort-by-2 [x]
  (sort-by first x))
(sort-by-1 ["dddd" "bb" "ccc" "a"])
(sort-by-1 ['(1 2) '(1) '(1 2 3)])
(sort-by-1 ["hola" "adios" "hi" "hoja"])

(defn sort-by-3 [x]
  (sort-by last x))
(sort-by-1 ["dddd" "bb" "ccc" "a" "adios" "comida" "beber"])
(sort-by-1 ['(1 2) '(1) '(1 2 3)])
(sort-by-1 ["hola" "adios" "hi" "hoja"])
;;--------------------------split-with..................................
(defn split-with-1 [x]
  (split-with zero? x))
(split-with-1 [12 4 -6 7 3 0 -1 -2])
(split-with-1 [-2 -5 -6 -8 0 32 -1 3 4 6 0 2 5 6 23])
(split-with-1 [8 0 1])

(defn split-with-2 [x]
  (split-with even? x))
(split-with-2 [12 4 -6 7 3 0 -1 -2])
(split-with-2 [-2 -5 -6 -8 0 32 -1 3 4 6 0 2 5 6 23])
(split-with-2 [8 0 1])

(defn split-with-3 [x]
  (split-with odd? x))
(split-with-3 [7 3 0 -1 -2])
(split-with-3 [3 4 6 0 2 5 6 23])
(split-with-3 [1 3 5 6 7 9])
;;--------------------------take..................................
(defn monkey-trouble [a-smile b-smile]
  (= a-smile b-smile))
(monkey-trouble false false)

(defn take-1 [x]
  (take 1 x))
(take-1 [7 3 0 -1 -2])
(take-1 "buenas tardes")
(take-1 [1 3 5 6 7 9])

(defn take-2 [x]
  (take 2 x))
(take-2 [7 3 0 -1 -2])
(take-2 "buenas tardes")
(take-2 [1 3 5 6 7 9])

(defn take-3 [x]
  (take 3 x))
(take-3 [7 3 0 -1 -2])
(take-3 "buenas tardes")
(take-3 [1 3 5 6 7 9])
;;--------------------------take-last..................................
(defn take-last-1 [x]
  (take-last 1 x))
(take-last-1 [7 3 0 -1 -2])
(take-last-1 "buenas tardes")
(take-last-1 [1 3 5 6 7 9])

(defn take-last-2 [x]
  (take-last 2 x))
(take-last-2 [7 3 0 -1 -2])
(take-last-2 "buenas tardes")
(take-last-2 [1 3 5 6 7 9])

(defn take-last-3 [x]
  (take-last 3 x))
(take-last-3 [7 3 0 -1 -2])
(take-last-3 "buenas tardes")
(take-last-3 [1 3 5 6 7 9])
;;--------------------------take-nth..................................
(defn take-3th-1 [x]
  (take-nth 3 x))
(take-3th-1 [7 3 0 -1 -2])
(take-3th-1 "buenas tardes")
(take-3th-1 [1 3 5 6 7 9])

(defn take-5th-2 [x]
  (take-nth 5 x))
(take-5th-2 [7 3 0 -1 -2])
(take-5th-2 "buenas tardes")
(take-5th-2 [1 3 5 6 7 9])

(defn take-4th-3 [x]
  (take-nth 4 x))
(take-4th-3 [7 3 0 -1 -2])
(take-4th-3 "buenas tardes")
(take-4th-3 [1 3 5 6 7 9])
;;--------------------------take-while..................................
(defn take-while-1 [x]
  (take-while neg? x))
(take-while-1 [12 4 -6 7 3 0 -1 -2])
(take-while-1 [-2 -5 -6 -8 0 32 -1 3 4 6 0 2 5 6 23])
(take-while-1 [-8 0 32 -1 3 4 1])

(defn take-while-2 [x]
  (take-while pos? x))
(take-while-2 [12 4 -6 7 3 0 -1 -2])
(take-while-2 [-2 -5 -6 -8 0 32 -1 3 4 6 0 2 5 6 23])
(take-while-2 [1 32 -1 3 4 1])
